import threading
import time
import collections
import cv2
import zmq
import statistics
import numpy as np
from utilities.utils import string_to_image


frames = collections.deque(maxlen=1)
modelpath = "models/medium/"

class ImageGrabber(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        print("Waiting for camera to connect...")
        context = zmq.Context()
        self.footage_socket = context.socket(zmq.SUB)
        self.footage_socket.bind('tcp://*:' + port)
        self.footage_socket.setsockopt_string(zmq.SUBSCRIBE, np.unicode(''))
        self.current_frame = None
        self.keep_running = True

    def run(self):
        global frames
        self.keep_running = True
        while self.footage_socket and self.keep_running:
            try:
                frame = self.footage_socket.recv_string()
                self.current_frame = string_to_image(frame)
                frames.append(self.current_frame)
                #time.sleep(0.001)
                #cv2.imshow('original', self.current_frame)
                #cv2.waitKey(1)

            except KeyboardInterrupt:
                cv2.destroyAllWindows()
                break
        print("Streaming Stopped!")


class ObjectDetector(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.cvNet = cv2.dnn.readNetFromTensorflow(modelpath+'frozen_inference_graph.pb',modelpath+'graph.pbtxt')
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.labels = list(line.strip() for line in open('models/annotations.txt'))
        self.colorLabels = self.get_spaced_colors(len(self.labels)+16)

        self.peoples = collections.deque(maxlen=9)

    def get_spaced_colors(self, n):
        max_value = 16581375  # 255**3
        interval = int(max_value / n)
        colors = [hex(I)[2:].zfill(6) for I in range(0, max_value, interval)]
        return [(int(i[:2], 16), int(i[2:4], 16), int(i[4:], 16)) for i in colors]

    def run(self):
        global frames
        while True:
            if (len(frames) > 0):
                img = frames.pop()
                rows = img.shape[0]
                cols = img.shape[1]
                self.cvNet.setInput(cv2.dnn.blobFromImage(img, size=(300, 300), swapRB=True, crop=False))
                cvOut = self.cvNet.forward()
                peoplecount = 0
                for detection in cvOut[0, 0, :, :]:
                    score = float(detection[2])

                    if score > 0.7:# and detection[1] == 1:
                        left = detection[3] * cols
                        top = detection[4] * rows
                        right = detection[5] * cols
                        bottom = detection[6] * rows
                        category = int(detection[1])-1
                        cv2.rectangle(img, (int(left), int(top)), (int(right), int(bottom)), self.colorLabels[category+3], thickness=2)
                        try:
                            cv2.putText(img, self.labels[category], (int(left)+5, int(top)+15), self.font, 0.5, self.colorLabels[category+3], 2,cv2.LINE_4)
                        except:
                            cv2.putText(img, "?", (int(left) + 5, int(top) + 15), self.font, 0.5,self.colorLabels[category+3], 2, cv2.LINE_4)
                        if detection[1] == 1:
                            peoplecount += 1

                self.peoples.append(peoplecount)
                try:
                    meanCount = statistics.mode(self.peoples)
                except:
                    meanCount = statistics.mean(self.peoples)

                cv2.putText(img, "Count: {0:}".format(meanCount), (5, 20), self.font, 0.7, (255, 255, 0), 2,cv2.LINE_4)
                cv2.imshow('computed', img)
                cv2.waitKey(1)



grabber = ImageGrabber('5555')
detector = ObjectDetector()
grabber.start()
detector.start()